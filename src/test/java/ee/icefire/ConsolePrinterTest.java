package ee.icefire;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.icefire.entity.Participant;

public class ConsolePrinterTest {

	private ConsolePrinter consolePrinter;

	@Before
	public void setUp() throws Exception {
		consolePrinter = new ConsolePrinter();
	}

	@Test
	public void testParticipantOrdering() {
		//given
		List<Participant> participants = new ArrayList<>();
		Participant winner = new Participant("winner", new ArrayList<>());
		winner.setScore(200);
		Participant secondPlace = new Participant("2ndPlace", new ArrayList<>());
		secondPlace.setScore(100);
		participants.add(secondPlace);
		participants.add(winner);

		//when
		List<Participant> result = consolePrinter.getWinningOrder(participants);

		//then
		List<Participant> expected = new ArrayList<>();
		expected.add(winner);
		expected.add(secondPlace);

		Assert.assertEquals(expected, result);

	}

}
