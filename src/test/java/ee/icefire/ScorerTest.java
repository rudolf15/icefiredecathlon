package ee.icefire;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ee.icefire.calculator.Calculator;
import ee.icefire.calculator.TrackEventsCalculator;
import ee.icefire.entity.Challenge;
import ee.icefire.entity.Event;
import ee.icefire.entity.Participant;

public class ScorerTest {

	private Scorer scorer;
	private List<Participant> participants;


	@Before
	public void setUp() throws Exception {
		Map<Event, Calculator> map = new HashMap<>();
		map.put(Event._100m, new TrackEventsCalculator(
						BigDecimal.valueOf(25.4347),
						BigDecimal.valueOf(18.00),
						BigDecimal.valueOf(1.81)));
		scorer = new Scorer(map);

		List<Challenge> challenges = new ArrayList<>();
		challenges.add(new Challenge(Event._100m, BigDecimal.valueOf(10.62)));
		participants = Collections.singletonList(new Participant("testPerson", challenges));

	}

	@Test
	public void testScoreIsSet() {
		//given

		//when
		scorer.scoreParticipants(participants);

		//then
		Integer expectedScore = 947;
		assertEquals(expectedScore, participants.get(0).getScore());
	}

}
