package ee.icefire;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ee.icefire.entity.Challenge;
import ee.icefire.entity.Event;
import ee.icefire.entity.Participant;

public class DataImporterTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private DataImporter dataImporter;
	private StringReader stringReader;

	@Before
	public void setUp() throws Exception {
		dataImporter = new DataImporter();
		stringReader = new StringReader("name,100m\ntestPerson,100.0");
	}

	@Test
	public void testInvalidHeaderEvent() throws IOException {
		//given
		stringReader = new StringReader("name,fail\ntest,10.0");
		exception.expect(IllegalStateException.class);
		exception.expectMessage("Got unsupported name: fail");

		//when
		dataImporter.importParticipantsFromStream(stringReader);

		//then
		//error is expected
	}

	@Test
	public void testValidParticipant() throws IOException {
		//given
		List<Participant> expected = new ArrayList<>();
		List<Challenge> expectedChallenge = new ArrayList<>();
		expectedChallenge.add(new Challenge(Event._100m, BigDecimal.valueOf(100.0)));
		Participant testPerson = new Participant("testPerson", expectedChallenge);
		expected.add(testPerson);

		//when
		List<Participant> participants = dataImporter.importParticipantsFromStream(stringReader);

		//then
		assertEquals(1, participants.size());
		assertEquals(expected, participants);
	}

}
