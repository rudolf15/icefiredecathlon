package ee.icefire.calculator;

import java.math.BigDecimal;

public interface Calculator {

	Integer calculate(BigDecimal result);

}
