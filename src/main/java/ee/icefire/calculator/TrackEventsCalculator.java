package ee.icefire.calculator;

import java.math.BigDecimal;

public class TrackEventsCalculator implements Calculator {

	private BigDecimal a;
	private BigDecimal b;
	private BigDecimal c;

	public TrackEventsCalculator(BigDecimal a, BigDecimal b, BigDecimal c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	/**
	 * P = a*(b - T) ** c
	 */
	public Integer calculate(BigDecimal variable) {
		if (variable.signum() == -1) {
			return 0;
		}
		BigDecimal subtract = b.subtract(variable);
		//losing some precision
		double pow = Math.pow(subtract.doubleValue(), c.doubleValue());
		int result = a.multiply(BigDecimal.valueOf(pow)).intValue();
		return Math.max(result, 0);
	}

}
