package ee.icefire.calculator;

import java.math.BigDecimal;

public class CalculatorFactory {

	public static Calculator getCalculator(CalculatorType type, BigDecimal a, BigDecimal b, BigDecimal c) {
		if (type == CalculatorType.RUN) {
			return new TrackEventsCalculator(a, b, c);
		}
		if (type == CalculatorType.JUMP) {
			return new JumpsAndThrowsCalculator(a, b, c);
		}
		throw new IllegalStateException("Unsupported calculator type. Got: " + type);
	}

	public enum CalculatorType {
		RUN, JUMP
	}

}
