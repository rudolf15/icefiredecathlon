package ee.icefire.entity;

import java.util.List;

public class Participant {

	private String name;
	private List<Challenge> challenges;
	private Integer score;

	public Participant(String name, List<Challenge> challenges) {
		this.name = name;
		this.challenges = challenges;
	}

	public String getName() {
		return name;
	}

	public List<Challenge> getChallenges() {
		return challenges;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Participant that = (Participant) o;

		if (name != null ? !name.equals(that.name) : that.name != null)
			return false;
		if (challenges != null ? !challenges.equals(that.challenges) : that.challenges != null)
			return false;
		return score != null ? score.equals(that.score) : that.score == null;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (challenges != null ? challenges.hashCode() : 0);
		result = 31 * result + (score != null ? score.hashCode() : 0);
		return result;
	}
}
