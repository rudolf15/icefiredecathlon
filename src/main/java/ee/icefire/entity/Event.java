package ee.icefire.entity;

public enum Event {
	_100m, _200m, _400m, _1500m, HURDLES, HIGH_JUMP, POLE_VAULT, LONG_JUMP, SHOT, DISCUS, JAVELIN
}
