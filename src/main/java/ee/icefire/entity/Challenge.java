package ee.icefire.entity;

import java.math.BigDecimal;

public class Challenge {

	private Event event;
	private BigDecimal result;

	public Challenge(Event event, BigDecimal result) {
		this.event = event;
		this.result = result;
	}

	public Event getEvent() {
		return event;
	}

	public BigDecimal getResult() {
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Challenge challenge = (Challenge) o;

		if (event != challenge.event)
			return false;
		return result != null ? result.equals(challenge.result) : challenge.result == null;
	}

	@Override
	public int hashCode() {
		int result1 = event != null ? event.hashCode() : 0;
		result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
		return result1;
	}
}
