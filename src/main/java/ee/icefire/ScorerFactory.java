package ee.icefire;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import ee.icefire.calculator.Calculator;
import ee.icefire.calculator.CalculatorFactory;
import ee.icefire.calculator.CalculatorFactory.CalculatorType;
import ee.icefire.entity.Event;

public class ScorerFactory {

	public static Scorer getScorer(ScorerType type) {
		Properties prop = new Properties();
		InputStream input = null;
		Map<Event, Calculator> scoreCalculators = new HashMap<>();
		try {

			String filename = getFileName(type);
			input = Application.class.getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				throw new IllegalStateException("Properties file was not found!");
			}
			prop.load(input);

			for (Entry<Object, Object> property : prop.entrySet()) {
				Calculator calculator = getCalculator((String)property.getValue());
				Event event = Event.valueOf((String) property.getKey());

				scoreCalculators.put(event, calculator);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return new Scorer(scoreCalculators);
	}

	private static Calculator getCalculator(String property) {
		String[] values = property.split(" ");
		CalculatorType calculatorType = CalculatorType.valueOf(values[0]);

		return CalculatorFactory.getCalculator(
						calculatorType,
						new BigDecimal(values[1]),
						new BigDecimal(values[2]),
						new BigDecimal(values[3]));
	}

	private static String getFileName(ScorerType type) {
		if (type == ScorerType.MEN) {
			return "mensEvents.properties";
		}
		if (type == ScorerType.WOMEN) {
			return "womensEvents.properties";
		}
		throw new IllegalStateException("Unsupported scoring type. Got: " + type);
	}

	public enum ScorerType {
		MEN, WOMEN
	}
}
