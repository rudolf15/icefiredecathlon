package ee.icefire;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ee.icefire.entity.Participant;

public class ConsolePrinter {

	public List<Participant> printScoresAsc(List<Participant> participants) {
		List<Participant> sorted = getWinningOrder(participants);
		int order = 1;
		for (Participant participant : sorted) {
			System.out.println(order++ + ". " + participant.getName() + "(" + participant.getScore() + ")");
		}
		return sorted;
	}

	public List<Participant> getWinningOrder(List<Participant> participants) {
		List<Participant> sorted = new ArrayList<>(participants);
		sorted.sort(Comparator.comparing(Participant::getScore).reversed());
		return sorted;
	}
}
