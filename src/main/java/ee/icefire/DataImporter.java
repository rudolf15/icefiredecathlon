package ee.icefire;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ee.icefire.entity.Challenge;
import ee.icefire.entity.Event;
import ee.icefire.entity.Participant;

public class DataImporter {

	public List<Participant> importParticipantsFromFile(String filePath) throws IOException {
		Reader reader = new InputStreamReader(new FileInputStream(filePath));
		List<Participant> participants = importParticipantsFromStream(reader);
		reader.close();
		return participants;
	}

	public List<Participant> importParticipantsFromStream(Reader reader) throws IOException {
		BufferedReader br = new BufferedReader(reader);

		List<Event> header = getHeader(br.readLine());

		List<Participant> participants = new ArrayList<>();
		String strLine;
		while ((strLine = br.readLine()) != null) {
			String[] split = strLine.split(",");

			String[] challengeResults = Arrays.copyOfRange(split, 1, split.length);
			List<Challenge> challenges = getChallenges(challengeResults, header);
			String participantName = split[0];
			Participant participant = new Participant(participantName, challenges);
			participants.add(participant);
		}
		return participants;
	}

	private List<Challenge> getChallenges(String[] split, List<Event> headers) {
		List<Challenge> challenges = new ArrayList<>();
		for (int i = 0; i < split.length; i++) {
			BigDecimal eventResult = getEventResult(split[i], headers.get(i));
			challenges.add(new Challenge(headers.get(i), eventResult));
		}
		return challenges;
	}

	private BigDecimal getEventResult(String s, Event event) {
		if (event == Event._1500m) {
			String[] time = s.split(":");
			BigDecimal minutesToSeconds = BigDecimal.valueOf(Integer.valueOf(time[0]) * 60);
			BigDecimal seconds = new BigDecimal(time[1]);
			return minutesToSeconds.add(seconds);
		}

		return new BigDecimal(s);
	}

	private List<Event> getHeader(String s) {
		String[] headerNames = s.split(",");
		List<Event> events = new ArrayList<>();
		//start from 1. 0 is "name"
		for (int i = 1; i < headerNames.length; i++) {
			events.add(getEvent(headerNames[i]));
		}
		return events;
	}

	private Event getEvent(String name) {
		switch (name) {
		case "100m":
			return Event._100m;
		case "200m":
			return Event._200m;
		case "400m":
			return Event._400m;
		case "1500m":
			return Event._1500m;
		case "Hurdles":
			return Event.HURDLES;
		case "High jump(cm)":
			return Event.HIGH_JUMP;
		case "Pole vault(cm)":
			return Event.POLE_VAULT;
		case "Long jump(cm)":
			return Event.LONG_JUMP;
		case "Shot":
			return Event.SHOT;
		case "Discus":
			return Event.DISCUS;
		case "Javelin":
			return Event.JAVELIN;
		}
		throw new IllegalStateException("Got unsupported name: " + name);
	}
}
