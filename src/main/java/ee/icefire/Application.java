package ee.icefire;

import java.io.IOException;
import java.util.List;

import ee.icefire.ScorerFactory.ScorerType;
import ee.icefire.entity.Participant;

public class Application {

	private final Scorer scorer;
	private final ConsolePrinter consolePrinter;
	private final DataImporter dataImporter;

	public Application(Scorer scorer, ConsolePrinter consolePrinter, DataImporter dataImporter) {
		this.scorer = scorer;
		this.consolePrinter = consolePrinter;
		this.dataImporter = dataImporter;
	}

	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
			System.out.println("2 parameters are expected: Run java -jar decathlon-1.0-SNAPSHOT.jar <filename> <[MEN/WOMEN]>");
			return;
		}
		String filepath = args[0];
		ScorerType type = ScorerType.valueOf(args[1]);
		new Application(ScorerFactory.getScorer(type), new ConsolePrinter(), new DataImporter()).run(filepath);
	}

	public void run(String filepath) throws IOException {
		List<Participant> participants = dataImporter.importParticipantsFromFile(filepath);
		scorer.scoreParticipants(participants);
		consolePrinter.printScoresAsc(participants);
	}

}
