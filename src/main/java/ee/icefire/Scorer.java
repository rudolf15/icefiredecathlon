package ee.icefire;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import ee.icefire.calculator.Calculator;
import ee.icefire.entity.Challenge;
import ee.icefire.entity.Event;
import ee.icefire.entity.Participant;

public class Scorer {

	private final Map<Event, Calculator> calculators;

	public Scorer(Map<Event, Calculator> calculators) {
		this.calculators = Collections.unmodifiableMap(calculators);
	}

	public void scoreParticipants(List<Participant> participants) {
		for (Participant participant : participants) {
			Integer totalScore = 0;
			for (Challenge challenge : participant.getChallenges()) {
				Integer score = calculators.get(challenge.getEvent()).calculate(challenge.getResult());
				totalScore += score;
			}
			participant.setScore(totalScore);
		}

	}

}
