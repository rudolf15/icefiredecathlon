For the sake of easy usage even the compiled files are in this repository.

	1. Download project
	2. (optional) Build project: ./gradlew clean build
	
	Test report is in: ./build/reports/tests/test/index.html

	3. Program takes 2 parameters: <filepath> and score type (MEN or WOMEN)

	Run with command: java -jar build/libs/decathlon-1.0-SNAPSHOT.jar data.txt MEN

Test data is provided in file "data.txt"
CSV separator is ","